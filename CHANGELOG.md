# [1.9.0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.8.0...v1.9.0) (2022-06-06)


### Features

* add schema validation for league resources ([bb9eeca](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/bb9eecacb52737958c335cc3cdb52d1deb840a20))
* add schema validation to team resources ([a750df0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/a750df015eb790abcee58f53dd15a070c051d67e))
* add serializers ([9218a18](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/9218a1878429db8b207975a90517c57ff8642428))

# [1.8.0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.7.0...v1.8.0) (2022-05-26)


### Features

* add member relation to team entity ([fe7dd2a](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/fe7dd2a49e9378082b0d3905563e8387baa2de23))

# [1.7.0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.6.4...v1.7.0) (2022-05-25)


### Features

* add team resources ([20ffa08](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/20ffa0879b4566508fb635dd01ac9d276b1b9db2))

## [1.6.4](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.6.3...v1.6.4) (2022-05-25)


### Bug Fixes

* move old sequelize migrations ([e4c26af](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/e4c26afa3ec6d02ecdfab1c2c9a18b94d331f300))

## [1.6.3](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.6.2...v1.6.3) (2022-05-25)


### Bug Fixes

* update prod db configs ([07d2c7e](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/07d2c7eeca6b9ed1281ecc0f69dc0eed5a1ac62a))

## [1.6.2](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.6.1...v1.6.2) (2022-05-25)


### Bug Fixes

* add ssl property to db configs ([435f5b1](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/435f5b157be01207c14fadfcab66642158eaeb6d))

## [1.6.1](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.6.0...v1.6.1) (2022-05-24)


### Bug Fixes

* update logger configs ([700ba08](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/700ba08f131296b991e908e7cb00bc64d5e22793))

# [1.6.0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/compare/v1.5.0...v1.6.0) (2022-05-24)


### Features

* add league resources ([d759fa0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/d759fa0d276da8af9b2445bbbdfca75a20355b7f))
* add new orm ([1b79b9e](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/1b79b9e26f4a7d4c445f53c44c0b7ba61213171a))
* updating a lot of things ([e2dd4b7](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-api/commit/e2dd4b755b10917f3322cd141b23b68ba256f697))

# [1.5.0](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.4.0...v1.5.0) (2021-07-18)

### Features

- add custom errors for api ([e99c08c](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/e99c08c4217701eec1a0a6bee26af0d1e96bf766))
- add email to member creation ([c9b2fde](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/c9b2fde1e8437d02c5fd82bad17614a8d30ff580))
- add repository mode ([432dac2](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/432dac2d05f243c5920647ac835b02a3fc7fd92d))

# [1.4.0](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.3.0...v1.4.0) (2021-06-04)

### Features

- add password reset workflow ([57d9ac3](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/57d9ac3d42f95ab957c5af60c17edf99450ecf7e))

# [1.3.0](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.2.0...v1.3.0) (2021-06-01)

### Features

- **service:** upload images to S3 bucket ([c90b63d](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/c90b63d76fd5b31658c0861327acc041282760aa))

# [1.2.0](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.1.1...v1.2.0) (2021-05-08)

### Features

- **model:** add league ([c870520](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/c87052042d295f16485587cfece1d7140879c652))
- **model:** add member role join ([1aa6cbb](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/1aa6cbbf73e33b40b5f6be8c38de0a1cebdc7051))
- **model:** add team ([e6d3e33](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/e6d3e330d1547038c804c728857508dc43f0e413))

## [1.1.1](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.1.0...v1.1.1) (2021-04-17)

### Bug Fixes

- use callback method for app instance ([13d024f](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/13d024f6c7944aaded825196f32c157d8124b506))

# [1.1.0](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.6...v1.1.0) (2021-03-20)

### Features

- configure Sequelize to use transactions ([4b51c23](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/4b51c2377b88974727c5f9ac42c0d7cf908f4b02))

## [1.0.6](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.5...v1.0.6) (2021-03-17)

### Bug Fixes

- **no-release:** re-add dialectOptions to configs ([678a51f](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/678a51fcf949d6a6a0cc5284e976bb45d93305f0))
- **pm2:** update configs ([5d5d786](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/5d5d7867134e636754e7a46f62a1e877ef8ffcdf))

## [1.0.5](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.4...v1.0.5) (2021-03-17)

### Bug Fixes

- update db configs ([e04aecb](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/e04aecb86f750993c18c3720d0fe764fa11b89b6))

## [1.0.4](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.3...v1.0.4) (2021-03-17)

### Bug Fixes

- add dialectOptions to db config ([1da036f](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/1da036fc2fecba18f481b6a9ac168ffb5fa5e218))

## [1.0.3](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.2...v1.0.3) (2021-03-17)

### Bug Fixes

- update db configs ([b44be7c](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/b44be7c25369537bd261be6982e38f376de69ee2))

## [1.0.2](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.1...v1.0.2) (2021-03-17)

### Bug Fixes

- correct typescript build error ([ce106cb](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/ce106cb0d476183ade41c92c2740675d84d4846f))

## [1.0.1](https://gitlab.com/mrbernnz/fantasy-draft-board-api/compare/v1.0.0...v1.0.1) (2021-03-17)

### Bug Fixes

- **deploy:** remove optional operator ([64e452e](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/64e452e9acc9bcdbb7ec8c1d60b3b55d38296296))

# 1.0.0 (2021-03-16)

### Features

- add data validator to controller ([86b29ca](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/86b29ca51d13980731b8d9c01b0af54c2ba45a94))
- add error objects to router ([78675a8](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/78675a80b1bacb754a448c1179163a16ea0aaaef))
- add initial middleware ([8b0dc40](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/8b0dc407258ec9b74069277823c567b629a7ddb4))
- add ORM for db ([0b14723](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/0b1472348dff156286ec3564da99a68d8e573771))
- auth-layer ([0cf6f80](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/0cf6f80252ae94784bd1adcff3d8c483137c0b93))
- create controller for fantasy owner ([bc3646f](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/bc3646ff07e61a2ef3d1f8795f00a9a5a61437d3))
- create fantasy owner model ([a4e5606](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/a4e56062ae8cc14ee5d54495a63a3768c99fbd1c))
- FantasyOwner changes ([7cf9382](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/7cf93826de9dffba3c364be8e2453ca9377a6669))
- remap modules ([77ad21a](https://gitlab.com/mrbernnz/fantasy-draft-board-api/commit/77ad21a93efb6d672f8f6a85cbaabc796c458fed))
