type E = 'test' | 'development' | 'production';

declare namespace NodeJS {
  export interface ProcessEnv {
    NODE_ENV: E;
    PORT: string;
    DB_URL: string;
    DB_DEV_URL: string;
    DB_TEST_URL: string;
    AWS_ACCESS_KEY: string;
    AWS_SECRET_ACCESS_KEY: string;
    S3_BUCKET_NAME: string;
    CLIENT_URL: string;
  }
}
