/** @type {import('@jest/types').Config.InitialOptions} */
/** @type {import('ts-jest').InitialOptionsTsJest} */
module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.ts',
    '!src/**/*.test.*',
    '!src/testUtils/**',
    '!src/base'
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/stryker-tmp/',
    '<rootDir>/node_modules',
    '<rootDir>/dist/',
    '<rootDir>/src/db',
    '<rootDir>/src/base'
  ],
  globals: {
    'ts-jest': { tsconfig: 'tsconfig.dev.json' }
  },
  moduleFileExtensions: ['ts', 'js', 'node'],
  preset: 'ts-jest',
  reporters: [
    'default',
    [
      'jest-stare',
      {
        coverageLink: 'lcov-report/index.html',
        resultDir: 'coverage',
        reportHeadline: 'Fantasy Draft Board API',
        reportTitle: 'Fantasy Draft Board API'
      }
    ]
  ],
  setupFilesAfterEnv: ['jest-extended/all'],
  testEnvironment: 'node',
  testPathIgnorePatterns: ['stryker-tmp', 'node_modules', 'dist'],
  testRegex: '(\\.|./src)(test|spec)\\.ts$',
  transform: {
    '^.+\\.(ts)$': 'ts-jest'
  }
};
