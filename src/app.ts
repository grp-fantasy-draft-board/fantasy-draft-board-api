// FIXME:  fix newrelic error
// import 'newrelic';
import Koa, { Context } from 'koa';
import connectDatabase from './configs/dataSource';
import router from './configs/routes';
import ApiError from './errors/apiError';
import middleware from './middleware';

connectDatabase();

const app: Koa = new Koa();

app.silent = true;
app
  .use(middleware)
  .use(router)
  .on('error', (err: Error, ctx: Context) => {
    ctx.log.error(new ApiError(`Draft Board Api Error: ${err.message}`, err));
  });

export default app;
