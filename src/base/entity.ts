import { customAlphabet } from 'nanoid';
import alphanumeric from 'nanoid-dictionary/alphanumeric';
import {
  BeforeInsert,
  BeforeUpdate,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn
} from 'typeorm';

export default abstract class Base {
  @PrimaryColumn({ select: true })
  id: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: Date;

  @BeforeInsert()
  setTimestamps() {
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }

  @BeforeUpdate()
  updateTimestamp() {
    this.updatedAt = new Date();
  }

  @BeforeInsert()
  generateId(): void {
    const size = 12;
    const nanoid = customAlphabet(alphanumeric, size);
    this.id = nanoid();
  }
}
