import { DataSource, DataSourceOptions } from 'typeorm';
import { logger } from '../utils';
import databaseEnvironments from './databases';
import DatabaseError from '../errors/databaseError';

const retrieveDBConfigs = () => {
  const { NODE_ENV } = process.env;
  const env = NODE_ENV ?? 'development';
  return databaseEnvironments[env];
};

const database = retrieveDBConfigs();

const dataSourceOpts: DataSourceOptions = {
  ...database
};

const dataSource = new DataSource(dataSourceOpts);

export { dataSource, dataSourceOpts };

export default function connectDatabase() {
  dataSource
    .initialize()
    .then(() => {
      logger.info('FDB database connection established.');
    })
    .catch((error) => {
      logger.error(
        new DatabaseError('FDB database connection not established', error)
      );
    });
}
