import { DatabaseLogger } from '../utils';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

const { DB_URL, DB_DEV_URL, DB_TEST_URL } = process.env;

interface Environments {
  development: string;
  test: string;
  production: string;
}

type E<Type> = {
  [Property in keyof Type]: PostgresConnectionOptions;
};

type DatabaseEnvironments = E<Environments>;

const databaseEnvironments: DatabaseEnvironments = {
  development: {
    type: 'postgres',
    url: DB_DEV_URL,
    logging: true,
    logger: new DatabaseLogger(),
    entities: ['src/**/entity.ts'],
    migrations: ['src/db/migrations/*.ts']
  },
  test: {
    type: 'postgres',
    url: DB_TEST_URL,
    synchronize: true,
    dropSchema: true,
    entities: ['src/**/entity.ts'],
    migrations: ['src/db/migrations/*.ts']
  },
  production: {
    type: 'postgres',
    url: DB_URL,
    logging: true,
    logger: new DatabaseLogger(),
    entities: ['dist/**/entity.js'],
    migrations: ['dist/db/migrations/*.js'],
    ssl: {
      rejectUnauthorized: false
    }
  }
};

export default databaseEnvironments;
