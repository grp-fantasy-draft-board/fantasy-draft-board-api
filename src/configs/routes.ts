import Boom from '@hapi/boom';
import Router, { RouterAllowedMethodsOptions } from '@koa/router';
import compose from 'koa-compose';
import { koaSwagger } from 'koa2-swagger-ui';
import path from 'path';
import yamljs from 'yamljs';
import * as leagueController from '../league/controller';
import * as memberController from '../member/controller';
import { jwtAuthentication } from '../middleware';
import * as teamController from '../team/controller';

const member: Router = new Router({ prefix: '/members' });
const league: Router = new Router({ prefix: '/leagues' });
const team: Router = new Router({ prefix: '/teams' });
const api: Router = new Router({ prefix: '/api/v1' });
const base: Router = new Router();

member
  .post('/', memberController.registerMember)
  .get('/', memberController.retrieveMembers)
  .param('member', memberController.validateMemberId)
  .get('/:member', memberController.retrieveMember)
  .get('/:member/image', memberController.retrieveImageUrl)
  .post('/:member/leagues', memberController.createMemberLeague)
  .get('/:member/leagues', memberController.retrieveMemberLeagues);

league
  .param('league', leagueController.validateLeaugeId)
  .get('/:league', leagueController.retrieveMemberLeague)
  .patch('/:league', leagueController.editMemberLeague)
  .delete('/:league', leagueController.removeMemberLeague)
  .post('/:league/teams', leagueController.createLeagueTeam)
  .get('/:league/teams', leagueController.retrieveLeagueTeams);

team
  .param('team', teamController.checkTeamId)
  .get('/:team', teamController.retrieveLeagueTeam)
  .patch('/:team', teamController.editLeagueTeam)
  .delete('/:team', teamController.removeLeagueTeam);

const spec = yamljs.load(path.join(__dirname, '..', '..', 'openapi.yml'));

base
  .get('/', (ctx) => {
    ctx.redirect('/docs');
  })
  .get(
    '/docs',
    koaSwagger({
      title: 'Fantasy Draft Board API',
      routePrefix: false,
      swaggerOptions: { spec },
      hideTopbar: true
    })
  );

api.use(jwtAuthentication, member.routes(), league.routes(), team.routes());
base.use(koaSwagger({ swaggerOptions: { spec } }), api.routes());

const opts: RouterAllowedMethodsOptions = {
  throw: true,
  notImplemented: () => Boom.notImplemented(),
  methodNotAllowed: () => Boom.methodNotAllowed()
};

export default compose([base.routes(), base.allowedMethods(opts)]);
