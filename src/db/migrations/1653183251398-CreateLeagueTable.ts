import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex
} from 'typeorm';

export class CreateLeagueTable1653183251398 implements MigrationInterface {
  name = 'CreateLeagueTable1653183251398';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'leagues',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            isNullable: false
          },
          {
            name: 'name',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'image_url',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'member_id',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
            isNullable: false
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
            isNullable: false
          }
        ]
      }),
      true
    );

    await queryRunner.createIndex(
      'leagues',
      new TableIndex({
        name: 'idx_league_name',
        columnNames: ['name']
      })
    );

    await queryRunner.createForeignKey(
      'leagues',
      new TableForeignKey({
        columnNames: ['member_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'members',
        onDelete: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('leagues');
    const foreignKey = table?.foreignKeys.find((foreignKey) =>
      foreignKey.columnNames.includes('member_id')
    );

    if (foreignKey) {
      await queryRunner.dropForeignKey('leagues', foreignKey);
    }
    await queryRunner.dropIndex('leagues', 'idx_league_name');
    await queryRunner.dropTable('leagues');
  }
}
