import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex
} from 'typeorm';

export class createTeamTable1653427057939 implements MigrationInterface {
  name = 'createTeamTable1653427057939';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'teams',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            isNullable: false
          },
          {
            name: 'name',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'image_url',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'league_id',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'member_id',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
            isNullable: false
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
            isNullable: false
          }
        ]
      })
    );

    await queryRunner.createIndex(
      'teams',
      new TableIndex({
        name: 'idx_team_name',
        columnNames: ['name']
      })
    );

    await queryRunner.createForeignKey(
      'teams',
      new TableForeignKey({
        columnNames: ['member_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'members',
        onDelete: 'CASCADE'
      })
    );

    await queryRunner.createForeignKey(
      'teams',
      new TableForeignKey({
        columnNames: ['league_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'leagues',
        onDelete: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('teams');
    const leagueFK = table?.foreignKeys.find((foreignKey) =>
      foreignKey.columnNames.includes('league_id')
    );
    const memberFK = table?.foreignKeys.find((foreignKey) =>
      foreignKey.columnNames.includes('member_id')
    );

    if (leagueFK && memberFK) {
      await queryRunner.dropForeignKey('teams', leagueFK);
      await queryRunner.dropForeignKey('teams', memberFK);
    }
    await queryRunner.dropIndex('teams', 'idx_team_name');
    await queryRunner.dropTable('teams');
  }
}
