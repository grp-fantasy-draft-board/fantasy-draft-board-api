'use strict';

const ADMIN = 'ADMIN';
const MODERATOR = 'MODERATOR';
const MANAGER = 'MANAGER';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('roles', {
        id: {
          allowNull: false,
          type: Sequelize.STRING,
          primaryKey: true
        },
        designation: {
          allowNull: false,
          type: Sequelize.ENUM({ values: [ADMIN, MODERATOR, MANAGER] })
        },
        created_at: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          type: Sequelize.DATE
        }
      })
      .then(() => queryInterface.addIndex('roles', ['designation']));
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('roles');
  }
};
