'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('password_resets', {
      id: {
        allowNull: false,
        type: Sequelize.STRING,
        primaryKey: true
      },
      token: {
        allowNull: false,
        type: Sequelize.STRING
      },
      expires_in: {
        allowNull: false,
        type: Sequelize.BIGINT
      },
      member_id: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'members',
          key: 'id',
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('password_resets');
  }
};
