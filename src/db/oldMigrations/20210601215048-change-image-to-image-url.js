'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn('members', 'image', 'image_url');
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn('members', 'image_url', 'image');
  }
};
