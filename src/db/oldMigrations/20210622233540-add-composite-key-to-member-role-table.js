'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.addConstraint('roles', {
      fields: ['designation'],
      type: 'check',
      name: 'custom_check_role_constraint',
      where: {
        designation: ['ADMIN', 'MODERATOR', 'MANAGER']
      }
    });
  },

  down: (queryInterface) => {
    return queryInterface.removeConstraint(
      'roles',
      'custom_check_role_constraint'
    );
  }
};
