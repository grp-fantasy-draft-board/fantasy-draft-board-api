'use strict';
const { customAlphabet } = require('nanoid');
const alphanumeric = require('nanoid-dictionary/alphanumeric');

const size = 12;
const nanoid = customAlphabet(alphanumeric, size);

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert(
      'roles',
      [
        {
          id: nanoid(),
          designation: 'ADMIN',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: nanoid(),
          designation: 'MODERATOR',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: nanoid(),
          designation: 'MANAGER',
          created_at: new Date(),
          updated_at: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('fdb_roles', null, {});
  }
};
