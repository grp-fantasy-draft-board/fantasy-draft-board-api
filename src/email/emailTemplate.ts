import Mailgen, { Content } from 'mailgen';
import Member from '../member/entity';

const emailGenerator: Mailgen = new Mailgen({
  theme: 'default',
  product: {
    name: 'Fantasy Draft Board',
    link: 'https://fantasy-draft-board.netlify.app/'
    // logo: 'https://mailgen.js/img/logo.png'
  }
});

export function newFantasyLeagueMemberEmailTemplate(
  member: Member
  // link: string | undefined
): { emailBody: any; emailText: any } {
  const email: Content = {
    body: {
      name: member.firstName,
      intro: "You've been added to your fantasy league draft board."
      // action: {
      //   instructions:
      //     'Click the button below to start creating your draft leagues.',
      //   button: {
      //     color: '#22BC66',
      //     text: 'Leagues',
      //     link
      //   }
      // }
      // outro:
      //   'If you did not request a password reset, no further action is required on your part.'
    }
  };

  const emailBody: any = emailGenerator.generate(email);
  const emailText: any = emailGenerator.generatePlaintext(email);

  return { emailBody, emailText };
}
