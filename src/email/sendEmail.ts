import { createTransport, Transporter } from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import pino, { Logger } from 'pino';
import ApiError from '../errors/apiError';
import Member from '../member/entity';
import { newFantasyLeagueMemberEmailTemplate } from './emailTemplate';

const logger: Logger = pino();

const { MAIL_HOST } = process.env;

function sendEmailToMember(
  member: Member,
  subject: string,
  emailBody: any,
  emailText: any
): void {
  const transporter: Transporter<SMTPTransport.SentMessageInfo> = createTransport(
    {
      host: MAIL_HOST,
      port: 2525,
      auth: {
        user: '97d58c85dd959e',
        pass: '2198c9dec31797'
      }
    }
  );

  transporter.sendMail(
    {
      from: 'no-reply@fantasydraftboard.com',
      to: member.email,
      subject,
      html: emailBody,
      text: emailText
    },
    function (err: Error | null) {
      if (err) throw new ApiError(err.message, 500, err);
      logger.info('Message sent successfully.');
    }
  );
}

export function sendNewOwnerEmail(member: Member): void {
  const { emailBody, emailText } = newFantasyLeagueMemberEmailTemplate(member);
  const subject: string = 'Welcome to Fantasy Draft Board!';
  sendEmailToMember(member, subject, emailBody, emailText);
}
