import ApiError from './apiError';

describe('Api Error', () => {
  let error: ApiError;
  beforeAll(() => {
    error = new ApiError('Error', 404);
  });

  it('should return an error message', () => {
    expect(error.message).toEqual('Error');
  });

  it('should return error status code', () => {
    expect(error.status).toEqual(404);
  });

  describe('when no status code is provided', () => {
    it('should return 500 error status code', () => {
      error = new ApiError('Error');
      expect(error.status).toEqual(500);
    });
  });
});
