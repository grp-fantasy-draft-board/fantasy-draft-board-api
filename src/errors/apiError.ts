export default class ApiError extends Error {
  status?: number;
  error?: Error;

  constructor(message: string, ...args: Array<number | Error>) {
    super(message);

    this.status = 500;
    this.mapArgs(args);
    this.name = this.constructor.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    }
  }

  mapArgs(args: any[]) {
    args.forEach((arg: number | Error) => {
      switch (typeof arg) {
        case 'number':
          this.status = arg;
          break;
        case 'object':
          this.error = arg;
          this.stack = this.error?.stack;
          break;
      }
    });
  }
}
