import DatabaseError from './databaseError';

describe('Database Error', () => {
  let error: DatabaseError;

  it('should return an error message', () => {
    error = new DatabaseError('Bad query error', 'null error for id attribute');
    expect(error.message).toEqual(
      'Bad query error: null error for id attribute'
    );

    error = new DatabaseError(
      'Bad query error',
      new Error('null error for id attribute')
    );
    expect(error.message).toEqual(
      'Bad query error: null error for id attribute'
    );
  });
});
