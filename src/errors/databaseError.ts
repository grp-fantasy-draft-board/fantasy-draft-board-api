import ApiError from './apiError';

export default class DatabaseError extends ApiError {
  constructor(message: string, error: string | Error) {
    const errMessage = typeof error === 'string' ? error : error.message;

    super(`${message}: ${errMessage}`);
    if (error instanceof Error) this.stack = error.stack;
  }
}
