import { Context, Next } from 'koa';
import { LeagueTeamRequest } from '../team/types';
import * as service from './service';
import { LeagueRequest } from './types';
import * as memberService from '../member/service';

export async function validateLeaugeId(
  id: string,
  ctx: Context,
  next: Next
): Promise<void> {
  ctx.league = await service.findLeague(id);
  ctx.assert(ctx.league, 404, 'Resource not found.');
  await next();
}

export function retrieveMemberLeague(ctx: Context): void {
  ctx.status = 200;
  ctx.body = ctx.league;
  ctx.set('Content-Type', 'application/vnd.api+json');
}

export async function editMemberLeague(ctx: Context): Promise<void> {
  const id: string = ctx.league.id;
  const leagueRequest: LeagueRequest = ctx.request.body;
  await service.updateLeague(id, leagueRequest);

  ctx.status = 204;
}

export async function removeMemberLeague(ctx: Context): Promise<void> {
  const id: string = ctx.league.id;
  await service.removeLeague(id);

  ctx.status = 204;
}

export const retrieveLeagueTeams = async (ctx: Context): Promise<void> => {
  ctx.status = 200;
  ctx.body = await service.getLeagueTeams(ctx.league.data.id);
  ctx.set('Content-Type', 'application/vnd.api+json');
};

export async function createLeagueTeam(ctx: Context): Promise<void> {
  const isNewManager = ctx.query.newMember;
  const id: string = ctx.league.id;
  const teamRequest: LeagueTeamRequest = ctx.request.body;

  if (isNewManager === 'true') {
    await memberService.createMember(teamRequest.member);
  }

  await service.createLeagueTeam(id, teamRequest);

  ctx.status = 201;
}
