import { IsUrl } from 'class-validator';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany
} from 'typeorm';
import Base from '../base/entity';
import Member from '../member/entity';
import Team from '../team/entity';

@Entity('leagues')
export default class League extends Base {
  @Column()
  @Index()
  name: string;

  @Column({ name: 'image_url' })
  @IsUrl()
  imageUrl: string;

  @Column({ name: 'member_id' })
  memberId: string;

  @ManyToOne(() => Member, { cascade: true })
  @JoinColumn({ name: 'member_id' })
  member: Member;

  @OneToMany(() => Team, (team) => team.league)
  teams: Team[];
}
