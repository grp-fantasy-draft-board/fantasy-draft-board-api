import { dataSource } from '../configs/dataSource';
import League from './entity';
import { CustomLeagueRepository, LeagueRequest } from './types';
import Member from '../member/entity';
import Team from '../team/entity';

const customLeagueRepository: CustomLeagueRepository = {
  findLeagueById: function (id: string) {
    return this.createQueryBuilder('league')
      .where('id = :id', { id })
      .getOneOrFail();
  },
  findLeagueByName: function (name: string) {
    return this.createQueryBuilder('league')
      .where('name = :name', { name })
      .getOneOrFail();
  },
  findLeagueTeams: function (id: string) {
    return this.createQueryBuilder()
      .relation(League, 'teams')
      .of(id)
      .loadMany();
  },
  createAndSave: async function (member: Member, leagueRequest: LeagueRequest) {
    const league = this.create({ ...leagueRequest, memberId: member.id });
    await this.save(league);
  },
  addTeam: async function (league: League, team: Team) {
    await this.createQueryBuilder()
      .relation(League, 'teams')
      .of(league)
      .add(team);
  },
  updateLeague: async function (id: string, leagueRequest: LeagueRequest) {
    await this.createQueryBuilder()
      .update(League)
      .set(leagueRequest)
      .where('id = :id', { id })
      .execute();
  },
  removeLeague: async function (id: string) {
    await this.createQueryBuilder()
      .delete()
      .from(League)
      .where('id = :id', { id })
      .execute();
  }
};

const LeagueRepository = dataSource
  .getRepository(League)
  .extend(customLeagueRepository);

export default LeagueRepository;
