import { Serializer as JSONAPISerializer } from 'jsonapi-serializer';

const LeagueSerializer = new JSONAPISerializer('leagues', {
  attributes: ['name', 'imageUrl', 'memberId'],
  keyForAttribute: 'camelCase'
});

export default LeagueSerializer;
