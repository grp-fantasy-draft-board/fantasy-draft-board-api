import { dataSource } from '../configs/dataSource';
import MemberRepository from '../member/repository';
import Team from '../team/entity';
import TeamRepository from '../team/repository';
import TeamSerializer from '../team/serializer';
import { LeagueTeamRequest } from '../team/types';
import { requestSchema, requestValidator } from '../utils';
import League from './entity';
import LeagueRepository from './repository';
import LeagueSerializer from './serializer';
import { LeagueRequest } from './types';

async function findLeagueById(id: string): Promise<League> {
  const league = await LeagueRepository.findLeagueById(id);
  return LeagueSerializer.serialize(league);
}

export async function findLeague(id: string): Promise<League> {
  await requestValidator(requestSchema.id, { id });
  return await findLeagueById(id);
}

export async function updateLeague(
  id: string,
  leagueRequest: LeagueRequest
): Promise<void> {
  await requestValidator(requestSchema.id, { id });
  await requestValidator(requestSchema.league, leagueRequest);
  await LeagueRepository.updateLeague(id, leagueRequest);
}

export async function removeLeague(id: string): Promise<void> {
  await requestValidator(requestSchema.id, { id });
  await LeagueRepository.removeLeague(id);
}

async function createTeam(
  teamRequest: LeagueTeamRequest,
  id: string
): Promise<void> {
  return await dataSource.transaction(async (manager) => {
    const memberRepository = manager.withRepository(MemberRepository);
    const leagueRepository = manager.withRepository(LeagueRepository);
    const teamRepository = manager.withRepository(TeamRepository);

    const leagueManager = await memberRepository.findByEmail(
      teamRequest.member.email
    );
    const league = await leagueRepository.findLeagueById(id);
    await teamRepository.createAndSave(leagueManager, league, teamRequest);
    const team = await teamRepository.findTeamByName(teamRequest.team.name);
    await teamRepository.addManager(team, leagueManager);
    await leagueRepository.addTeam(league, team);
  });
}

export async function createLeagueTeam(
  id: string,
  teamRequest: LeagueTeamRequest
): Promise<void> {
  await requestValidator(requestSchema.id, { id });
  await requestValidator(requestSchema.leagueTeam, teamRequest);
  await createTeam(teamRequest, id);
}

export async function getLeagueTeams(id: string): Promise<Team[]> {
  const teams = await LeagueRepository.findLeagueTeams(id);
  return TeamSerializer.serialize(teams);
}
