import Member from '../member/entity';
import Team from '../team/entity';
import League from './entity';

export interface LeagueRequest {
  name: string;
  imageUrl: string;
}

export interface CustomLeagueRepository {
  findLeagueById: (id: string) => Promise<League>;
  findLeagueByName: (name: string) => Promise<League>;
  findLeagueTeams: (id: string) => Promise<Team[]>;
  createAndSave: (
    member: Member,
    leagueRequest: LeagueRequest
  ) => Promise<void>;
  addTeam: (league: League, team: Team) => Promise<void>;
  updateLeague: (id: string, leagueRequest: LeagueRequest) => Promise<void>;
  removeLeague: (id: string) => Promise<void>;
}
