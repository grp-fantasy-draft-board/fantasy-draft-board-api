import { Next, Context } from 'koa';
import { MemberRequest } from './types';
import * as service from './service';
import getPresignedUploadUrl from '../services/aws';
import { LeagueRequest } from '../league/types';

export async function registerMember(ctx: Context): Promise<void> {
  const body: MemberRequest = ctx.request.body;
  await service.createMember(body);

  ctx.set('Cache-Control', 'no-store');
  ctx.set('Pragma', 'no-cache');
  ctx.status = 201;
}

export async function retrieveMembers(ctx: Context): Promise<void> {
  ctx.status = 200;
  ctx.body = await service.getMembers();
  ctx.set('Content-Type', 'application/vnd.api+json');
}

export async function validateMemberId(
  id: string,
  ctx: Context,
  next: Next
): Promise<void> {
  ctx.member = await service.getMember(id);
  ctx.assert(ctx.member, 404, 'Fantasy member not found.');
  await next();
}

export function retrieveMember(ctx: Context): void {
  ctx.status = 200;
  ctx.body = ctx.member;
  ctx.set('Content-Type', 'application/vnd.api+json');
}

export async function retrieveMemberLeagues(ctx: Context): Promise<void> {
  ctx.status = 200;
  ctx.body = await service.getMemberLeagues(ctx.member.data.id);
  ctx.set('Content-Type', 'application/vnd.api+json');
}

export async function createMemberLeague(ctx: Context): Promise<void> {
  const memberId: string = ctx.member.id;
  const leagueRequest: LeagueRequest = ctx.request.body;

  await service.createLeague(memberId, leagueRequest);

  ctx.status = 201;
}

export function retrieveImageUrl(ctx: Context): void {
  const directory = ctx.query.directory as string;
  const fileType = ctx.query.fileType as string;

  ctx.status = 200;
  ctx.body = getPresignedUploadUrl(directory, fileType);
}
