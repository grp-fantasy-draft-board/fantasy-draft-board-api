import { IsAlpha, IsEmail, IsUrl } from 'class-validator';
import {
  Column,
  Entity,
  BeforeInsert,
  ManyToMany,
  JoinTable,
  OneToMany
} from 'typeorm';
import Base from '../base/entity';
import { capitalize } from 'lodash';
import Role from '../role/entity';
import League from '../league/entity';

@Entity('members')
export default class Member extends Base {
  @Column({ name: 'first_name' })
  @IsAlpha()
  firstName: string;

  @Column({ name: 'last_name' })
  @IsAlpha()
  lastName: string;

  @Column()
  @IsEmail()
  email: string;

  @Column({ name: 'encrypted_password', select: false })
  password: string;

  @Column({ name: 'image_url' })
  @IsUrl()
  imageUrl: string;

  @ManyToMany(() => Role)
  @JoinTable({
    name: 'member_roles',
    joinColumn: {
      name: 'member_id'
    },
    inverseJoinColumn: {
      name: 'role_id'
    }
  })
  roles: Role[];

  @OneToMany(() => League, (league) => league.member)
  leagues: League[];

  @BeforeInsert()
  formatAttributes() {
    this.firstName = capitalize(this.firstName);
    this.lastName = capitalize(this.lastName);
    this.email = this.email.toLowerCase();
  }
}
