export const leagueMembersQuery = `SELECT
          Member.id,
          Member.first_name AS firstName,
          Member.last_name AS lastName,
          Member.email,
          Member.image_url AS imageUrl,
          array_agg(roles.name) AS memberRoles
        FROM
          members AS Member
          LEFT OUTER JOIN (
            member_roles
            INNER JOIN roles
            ON roles.id = member_roles.member_id
          ) ON Member.id = member_roles.role_id
        GROUP BY
          Member.id,
          firstName,
          lastName,
          email,
          imageUrl`;

export const leagueMemberQuery = `SELECT
        Member.id,
        Member.first_name AS firstName,
        Member.last_name AS lastName,
        Member.email,
        Member.image_url AS imageUrl,
        array_agg(roles.name) AS memberRoles
      FROM
        members AS Member
        LEFT OUTER JOIN (
          member_roles
          INNER JOIN roles
          ON roles.id = member_roles.member_id
        ) ON Member.id = member_roles.role_id
      WHERE
        Member.id = ?
      GROUP BY
        Member.id,
        firstName,
        lastName,
        email,
        imageUrl`;
