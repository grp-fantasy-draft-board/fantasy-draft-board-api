import { dataSource } from '../configs/dataSource';
import Member from './entity';
import { Designation } from '../role/enum';
import { MemberRequest, CustomMemberRepository } from './types';
import League from '../league/entity';
import RoleRepository from '../role/repository';

const customMemberRepository: CustomMemberRepository = {
  findManagers: function () {
    return this.createQueryBuilder('member')
      .leftJoin('member.roles', 'role')
      .where('role.designation = :designation', {
        designation: Designation.MANAGER
      })
      .getMany();
  },
  findOwner: function (id: string) {
    return this.createQueryBuilder('member')
      .leftJoin('member.roles', 'role')
      .where('member.id = :id', { id })
      .andWhere('role.designation = :designation', {
        designation: Designation.OWNER
      })
      .getOneOrFail();
  },
  findOwnerLeagues: function (id: string) {
    return this.createQueryBuilder()
      .relation(Member, 'leagues')
      .of(id)
      .loadMany();
  },
  findByEmail: function (email: string) {
    return this.createQueryBuilder('member')
      .where('email = :email', { email })
      .getOneOrFail();
  },
  createAndSave: async function (memberRequest: MemberRequest) {
    const member = this.create(memberRequest);
    await this.save(member);
  },
  addManagerRole: async function (member: Member) {
    const roles = await RoleRepository.findManagerRole();
    await this.createQueryBuilder()
      .relation(Member, 'roles')
      .of(member)
      .add(roles);
  },
  addLeague: async function (member: Member, league: League) {
    await this.createQueryBuilder()
      .relation(Member, 'leagues')
      .of(member)
      .add(league);
  }
};

const MemberRepository = dataSource
  .getRepository(Member)
  .extend(customMemberRepository);

export default MemberRepository;
