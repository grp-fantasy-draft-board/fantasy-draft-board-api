import { Serializer as JSONAPISerializer } from 'jsonapi-serializer';

const MemberSerializer = new JSONAPISerializer('members', {
  attributes: ['firstName', 'lastName', 'email', 'imageUrl']
});

export default MemberSerializer;
