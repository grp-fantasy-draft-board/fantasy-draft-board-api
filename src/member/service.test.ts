/// <reference path="../../global.d.ts" />

import { mock, mockClear, MockProxy } from 'jest-mock-extended';
import { Repository } from 'typeorm';
import { dataSource } from '../configs/dataSource';
import Role from '../role/entity';
import { CustomRoleRepository } from '../role/types';
import {
  createMember,
  generateMemberList,
  generateRoleList,
  memberFactory
} from '../testUtils/factories';
import Member from './entity';
import * as service from './service';
import { CustomMemberRepository } from './types';

describe.skip('Member Service', () => {
  // const mockDataSource: MockProxy<DataSource> = mock<DataSource>();
  const mockMemberRepo: MockProxy<
    Repository<Member> & CustomMemberRepository
  > = mock<Repository<Member> & CustomMemberRepository>();
  const mockRoleRepo: MockProxy<Repository<Role> & CustomRoleRepository> = mock<
    Repository<Role> & CustomRoleRepository
  >();

  beforeAll(async () => {
    await dataSource.initialize();
    // console.log(mockDataSource.initialize());
  });

  afterAll(async () => {
    await dataSource.destroy();
    // mockClear(mockDataSource);
  });

  beforeEach(() => {
    mockClear(mockMemberRepo);
    mockClear(mockRoleRepo);
  });

  it.each`
    firstName | expected
    ${null}   | ${'`firstName` must be a string'}
    ${''}     | ${'`firstName` is not allowed to be empty'}
  `(
    'should throw attribute validation error $expected',
    async ({ firstName, expected }) => {
      const factory = memberFactory({ firstName });
      expect.assertions(1);
      await expect(service.createMember(factory)).rejects.toThrow(expected);
    }
  );

  it.each`
    lastName | expected
    ${null}  | ${'`lastName` must be a string'}
    ${''}    | ${'`lastName` is not allowed to be empty'}
  `(
    'should throw attribute validation error $expected',
    async ({ lastName, expected }) => {
      const factory = memberFactory({ lastName });
      expect.assertions(1);
      await expect(service.createMember(factory)).rejects.toThrow(expected);
    }
  );

  it.each`
    email        | expected
    ${null}      | ${'`email` must be a string'}
    ${''}        | ${'`email` is not allowed to be empty'}
    ${'example'} | ${'`email` must be a valid email'}
  `(
    'should throw attribute validation error $expected',
    async ({ email, expected }) => {
      const factory = memberFactory({ email });
      expect.assertions(1);
      await expect(service.createMember(factory)).rejects.toThrow(expected);
    }
  );

  it.each`
    imageUrl               | expected
    ${null}                | ${'`imageUrl` must be a string'}
    ${''}                  | ${'`imageUrl` is not allowed to be empty'}
    ${'htp://example.com'} | ${'`imageUrl` must be a valid uri with a scheme matching the https? pattern'}
  `(
    'should throw attribute validation error $expected',
    async ({ imageUrl, expected }) => {
      const factory = memberFactory({ imageUrl });
      expect.assertions(1);
      await expect(service.createMember(factory)).rejects.toThrow(expected);
    }
  );

  it.skip('should create league member', async () => {
    const factory = memberFactory();
    const mbr = createMember(factory);
    const manager = generateRoleList('manager');

    mockMemberRepo.createAndSave.calledWith(factory).mockResolvedValue();
    mockMemberRepo.findByEmail.calledWith(factory.email).mockResolvedValue(mbr);
    mockMemberRepo.addManagerRole.calledWith(mbr).mockResolvedValue();
    mockRoleRepo.findManagerRole.mockResolvedValue(manager);

    expect.assertions(1);
    await expect(service.createMember(factory)).toResolve();
  });

  it.skip('should return an empty list', async () => {
    mockMemberRepo.findManagers.mockResolvedValue([]);

    expect.assertions(1);
    await expect(service.getMembers()).resolves.toBeEmpty();
  });

  it.skip('should return member list', async () => {
    const mbrs = generateMemberList();

    mockMemberRepo.findManagers.mockResolvedValue(mbrs);

    expect.assertions(1);
    await expect(service.getMembers()).resolves.toEqual(mbrs);
  });

  it.each`
    id          | expected
    ${null}     | ${'`id` must be a string'}
    ${''}       | ${'`id` is not allowed to be empty'}
    ${'123abc'} | ${'`id` length must be 12 characters long'}
  `('should throw validation error $expected', async ({ id, expected }) => {
    expect.assertions(1);
    await expect(service.getMember(id)).rejects.toThrow(expected);
  });

  it('should return null if member not found', async () => {
    mockMemberRepo.findOneBy
      .calledWith({ id: '123456abcdef' })
      .mockRejectedValue(null);

    expect.assertions(1);
    await expect(service.getMember('123456abcdef')).rejects.toThrow();
  });

  it.skip('should return a league member', async () => {
    const mbr = createMember();

    mockMemberRepo.findOneBy
      .calledWith({ id: '123456abcdef' })
      .mockResolvedValue(mbr);

    expect.assertions(1);
    await expect(service.getMember('123456abcdef')).resolves.toEqual(mbr);
  });
});
