import { dataSource } from '../configs/dataSource';
// import * as sendEmail from '../email/sendEmail';
import League from '../league/entity';
import LeagueRepository from '../league/repository';
import LeagueSerializer from '../league/serializer';
import { LeagueRequest } from '../league/types';
import { requestSchema, requestValidator } from '../utils';
import Member from './entity';
import MemberRepository from './repository';
import MemberSerializer from './serializer';
import { MemberRequest } from './types';

async function createMemberWithManagerRole(
  memberRequest: MemberRequest
): Promise<void> {
  return await dataSource.transaction(async (manager) => {
    const memberRepository = manager.withRepository(MemberRepository);
    await memberRepository.createAndSave(memberRequest);
    const newMember = await memberRepository.findByEmail(memberRequest.email);
    await memberRepository.addManagerRole(newMember);
  });
}

export async function createMember(
  memberRequest: MemberRequest
): Promise<void> {
  await requestValidator(requestSchema.member, memberRequest);
  await createMemberWithManagerRole(memberRequest);

  // FIXME: properly implement mailer service.
  // sendEmail.sendNewOwnerEmail(newMember);
}

export async function getMembers(): Promise<Member[]> {
  const members = await MemberRepository.findManagers();
  return MemberSerializer.serialize(members);
}

export async function getMember(id: string): Promise<Member> {
  await requestValidator(requestSchema.id, { id });
  const member = await MemberRepository.findOneByOrFail({ id });
  return MemberSerializer.serialize(member);
}

async function createMemberLeague(
  id: string,
  leagueRequest: LeagueRequest
): Promise<void> {
  return await dataSource.transaction(async (manager) => {
    const memberRepository = manager.withRepository(MemberRepository);
    const leagueRepository = manager.withRepository(LeagueRepository);
    const member = await memberRepository.findOwner(id);
    const league = await leagueRepository.findLeagueByName(leagueRequest.name);

    await leagueRepository.createAndSave(member, leagueRequest);
    await memberRepository.addLeague(member, league);
  });
}

export async function createLeague(
  id: string,
  leagueRequest: LeagueRequest
): Promise<void> {
  await requestValidator(requestSchema.id, { id });
  await requestValidator(requestSchema.league, leagueRequest);
  await createMemberLeague(id, leagueRequest);
}

export async function getMemberLeagues(id: string): Promise<League[]> {
  const memberLeagues = await MemberRepository.findOwnerLeagues(id);
  return LeagueSerializer.serialize(memberLeagues);
}
