import Member from './entity';
import League from '../league/entity';

export interface JwtResponse {
  access_token: string;
  token_type: string;
  expires_in: number;
}

export interface MemberRequest {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  imageUrl: string;
}

export interface CustomMemberRepository {
  findManagers: () => Promise<Member[]>;
  findOwner: (id: string) => Promise<Member>;
  findOwnerLeagues: (id: string) => Promise<League[]>;
  findByEmail: (email: string) => Promise<Member>;
  createAndSave: (body: MemberRequest) => Promise<void>;
  addManagerRole: (member: Member) => Promise<void>;
  addLeague: (member: Member, league: League) => Promise<void>;
}
