import passport from 'koa-passport';
import {
  Strategy as JwtStrategy,
  ExtractJwt,
  StrategyOptions,
  VerifyCallback,
  VerifiedCallback
} from 'passport-jwt';
import Member from '../member/entity';
import * as utils from '../utils';
import MemberRepository from '../member/repository';

passport.serializeUser(
  (
    fantasyOwner: Member,
    done: (err: any, id?: string | undefined) => void
  ): void => {
    const ownerId = fantasyOwner.id;
    done(null, ownerId);
  }
);

passport.deserializeUser(
  (id: string, done: (err: any, user?: any) => void): void => {
    try {
      const member = MemberRepository.findOneByOrFail({ id });
      done(null, member);
    } catch (err) {
      done(err);
    }
  }
);

const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: utils.privateKey
};

const verifyFantasyOwner: VerifyCallback = async (
  payload: any,
  done: VerifiedCallback
): Promise<void> => {
  try {
    const member = await MemberRepository.findOneBy({ id: payload.id });
    member ? done(null, member) : done(null, false);
  } catch (err) {
    done(err);
  }
};

const strategy = new JwtStrategy(opts, verifyFantasyOwner);

passport.use(strategy);
