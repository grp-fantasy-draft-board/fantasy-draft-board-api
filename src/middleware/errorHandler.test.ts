import errorHandler from './errorHandler';

describe('Exception Handling Middleware', () => {
  let ctx: any;
  beforeEach(() => {
    ctx = {
      app: {
        emit: jest.fn()
      }
    };
  });

  it('responds with default error', async () => {
    const next: any = jest.fn(() => {
      expect(ctx.status).toBe(500);
    });
    await errorHandler(ctx, next);
    expect(next).toHaveBeenCalledTimes(1);
  });
});
