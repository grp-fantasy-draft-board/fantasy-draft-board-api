import { Error as JSONAPIError } from 'jsonapi-serializer';
import { Next, Context } from 'koa';

export default async function errorHandler(
  ctx: Context,
  next: Next
): Promise<void> {
  try {
    await next();
  } catch (err) {
    switch (err.name) {
      case 'AuthenticationError': {
        ctx.status = 401;
        ctx.body = new JSONAPIError({
          title: 'Unauthorized',
          detail: 'Invalid authorization token provided.'
        });
        break;
      }
      case 'ValidationError': {
        const errors = err.details.map((detail: any) => ({
          title: 'Invalid Attribute',
          detail: detail.message,
          source: { pointer: `data/attributes/${detail.context.key}` }
        }));

        ctx.status = 422;
        ctx.body = new JSONAPIError(errors);
        break;
      }
      case 'EntityNotFoundError': {
        ctx.status = 404;
        ctx.body = new JSONAPIError({
          title: 'Not found',
          detail: 'Could not find any entity',
          source: { parameter: ':id' }
        });
        break;
      }
      default:
        ctx.status = err.statusCode ?? err.status ?? 500;
        ctx.body = err.message;
        break;
    }
    ctx.app.emit('error', err, ctx);
  }
}
