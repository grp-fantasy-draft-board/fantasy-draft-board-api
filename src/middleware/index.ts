import cors from '@koa/cors';
import { Context } from 'koa';
import bodyParser, { IKoaBodyOptions } from 'koa-body';
import compose from 'koa-compose';
import passport from 'koa-passport';
import { koaLogger } from '../utils';
import './authStrategy';
import errorHandler from './errorHandler';

const bodyParserOptions: IKoaBodyOptions = {
  onError(err: Error, ctx: Context) {
    ctx.throw(422, 'Body parse error', err);
  }
};

export const jwtAuthentication = passport.authenticate('jwt', {
  session: false,
  failWithError: true
});

export default compose([
  errorHandler,
  cors(),
  bodyParser(bodyParserOptions),
  passport.initialize(),
  koaLogger
]);
