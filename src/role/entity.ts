import { Column, Entity } from 'typeorm';
import Base from '../base/entity';
import { Designation } from './enum';

@Entity('roles')
export default class Role extends Base {
  @Column({ type: 'enum', enum: Designation })
  designation: Designation;
}
