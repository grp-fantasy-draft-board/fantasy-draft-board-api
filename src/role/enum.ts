export enum Designation {
  MODERATOR = 'MODERATOR',
  MANAGER = 'MANAGER',
  OWNER = 'OWNER'
}
