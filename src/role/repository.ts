import { dataSource } from '../configs/dataSource';
import Role from './entity';
import { Designation } from './enum';
import { CustomRoleRepository } from './types';

const customRoleRepository: CustomRoleRepository = {
  findManagerRole: function () {
    return this.createQueryBuilder('role')
      .where('designation = :designation', { designation: Designation.MANAGER })
      .getOne();
  }
};

const RoleRepository = dataSource
  .getRepository(Role)
  .extend(customRoleRepository);

export default RoleRepository;
