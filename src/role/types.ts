import Role from './entity';

export interface CustomRoleRepository {
  findManagerRole: () => Promise<Role[]>;
}
