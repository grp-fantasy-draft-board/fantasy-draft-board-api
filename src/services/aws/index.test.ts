/// <reference path="../../../global.d.ts" />

import getPresignedUploadUrl from './index';

describe.skip('AWS S3 Service Provider', () => {
  it('should return a image url', () => {
    const directory = 'members';
    const fileType = 'png';
    const signedUrl = getPresignedUploadUrl(directory, fileType);

    expect(signedUrl.signedRequest).toBeString();
    expect(signedUrl.url).toBeString();
  });
});
