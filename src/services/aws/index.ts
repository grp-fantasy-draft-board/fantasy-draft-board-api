import AWS, { S3 } from 'aws-sdk';
import { S3Configs, S3Params, SignedUrls } from './types';
import { customAlphabet } from 'nanoid';
import alphanumeric from 'nanoid-dictionary/alphanumeric';

function createS3Configs(
  directory: string,
  fileType: string,
  S3_BUCKET_NAME?: string
): S3Configs {
  const nanoid: string = customAlphabet(alphanumeric, 6)();
  const key: string = `${directory}/${nanoid}.${fileType}`;
  const mimeType: string = `image/${fileType}`;
  const s3Params: S3Params = {
    Bucket: S3_BUCKET_NAME,
    Key: key,
    Expires: 60,
    ContentType: mimeType,
    ACL: 'public-read'
  };

  return { s3Params, key };
}

function createSignedUrls(
  s3Params: S3Params,
  key: string,
  S3_BUCKET_NAME?: string
): SignedUrls {
  const s3 = new S3();
  const signedRequest = s3.getSignedUrl('putObject', s3Params);
  const url = `https://${S3_BUCKET_NAME}.s3.amazonaws.com/${key}`;
  return { signedRequest, url };
}

export default function getPresignedUploadUrl(
  directory: string,
  fileType: string
): SignedUrls {
  AWS.config.region = 'us-east-1';
  const { S3_BUCKET_NAME } = process.env;
  const { s3Params, key } = createS3Configs(
    directory,
    fileType,
    S3_BUCKET_NAME
  );

  return createSignedUrls(s3Params, key, S3_BUCKET_NAME);
}
