export interface S3Params {
  Bucket: string | undefined;
  Key: string;
  Expires: number;
  ContentType: string;
  ACL: string;
}

export interface SignedUrls {
  signedRequest: string;
  url: string;
}

export interface S3Configs {
  s3Params: S3Params;
  key: string;
}
