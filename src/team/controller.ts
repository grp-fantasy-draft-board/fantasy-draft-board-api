import { Context, Next } from 'koa';
import * as service from './service';
import { TeamRequest } from './types';

export async function checkTeamId(
  id: string,
  ctx: Context,
  next: Next
): Promise<void> {
  ctx.team = await service.getTeam(id);
  ctx.assert(ctx.team, 404, 'Resource not found.');
  await next();
}

export function retrieveLeagueTeam(ctx: Context): void {
  ctx.status = 200;
  ctx.body = ctx.team;
}

export async function editLeagueTeam(ctx: Context): Promise<void> {
  const id: string = ctx.team.id;
  const teamRequest: TeamRequest = ctx.request.body;

  await service.updateTeam(id, teamRequest);

  ctx.status = 204;
}

export async function removeLeagueTeam(ctx: Context): Promise<void> {
  const id = ctx.team.id;
  await service.removeTeam(id);
  ctx.status = 204;
}
