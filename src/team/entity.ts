import { IsUrl } from 'class-validator';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import Base from '../base/entity';
import League from '../league/entity';
import Member from '../member/entity';

@Entity('teams')
export default class Team extends Base {
  @Column()
  name: string;

  @Column({ name: 'image_url' })
  @IsUrl()
  imageUrl: string;

  @Column({ name: 'league_id' })
  leagueId: string;

  @Column({ name: 'member_id' })
  memberId: string;

  @ManyToOne(() => League, { cascade: true })
  @JoinColumn({ name: 'league_id' })
  league: League;

  @OneToOne(() => Member, { cascade: true })
  @JoinColumn({ name: 'member_id' })
  member: Member;
}
