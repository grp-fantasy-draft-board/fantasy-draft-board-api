import { dataSource } from '../configs/dataSource';
import League from '../league/entity';
import Member from '../member/entity';
import Team from './entity';
import { CustomTeamRepository, LeagueTeamRequest, TeamRequest } from './types';

const customTeamRepository: CustomTeamRepository = {
  createAndSave: async function (
    member: Member,
    league: League,
    teamRequest: LeagueTeamRequest
  ) {
    const team = this.create({
      ...teamRequest.team,
      leagueId: league.id,
      memberId: member.id
    });
    team.member = member;
    await this.save(team);
  },
  findTeamByName: function (name: string) {
    return this.createQueryBuilder('team')
      .where('name = :name', { name })
      .getOneOrFail();
  },
  findTeamById: function (id: string) {
    return this.createQueryBuilder('team')
      .where('id = :id', { id })
      .getOneOrFail();
  },
  updateTeam: async function (id: string, teamRequest: TeamRequest) {
    await this.createQueryBuilder()
      .update(Team)
      .set(teamRequest)
      .where('id = :id', { id })
      .execute();
  },
  removeTeam: async function (id: string) {
    await this.createQueryBuilder()
      .delete()
      .from(Team)
      .where('id = :id', { id })
      .execute();
  },
  addManager: async function (team: Team, member: Member) {
    await this.createQueryBuilder()
      .relation(Team, 'member')
      .of(team)
      .set(member);
  }
};

const TeamRepository = dataSource
  .getRepository(Team)
  .extend(customTeamRepository);

export default TeamRepository;
