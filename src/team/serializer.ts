import { Serializer as JSONAPISerializer } from 'jsonapi-serializer';

const TeamSerializer = new JSONAPISerializer('members', {
  attributes: ['name', 'imageUrl', 'memberId', 'leagueId'],
  keyForAttribute: 'camelCase'
});

export default TeamSerializer;
