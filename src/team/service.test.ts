/// <reference path="../../global.d.ts" />

import { MockProxy, mock, mockClear } from 'jest-mock-extended';
import { Repository } from 'typeorm';
import Team from './entity';
import { CustomTeamRepository } from './types';
import * as service from './service';
import ApiError from '../errors/apiError';

describe('Team Services', () => {
  const mockTeamRepository: MockProxy<
    Repository<Team> & CustomTeamRepository
  > = mock<Repository<Team> & CustomTeamRepository>();

  beforeEach(() => {
    mockClear(mockTeamRepository);
  });

  it('throw error because a team was not found', async () => {
    mockTeamRepository.findTeamById
      .calledWith('133534567868')
      .mockRejectedValue(new ApiError('Team not found'));

    expect.assertions(1);
    await expect(service.getTeam).rejects.toThrow();
  });
});
