import { requestSchema, requestValidator } from '../utils';
import Team from './entity';
import TeamRepository from './repository';
import TeamSerializer from './serializer';
import { TeamRequest } from './types';

export async function getTeam(id: string): Promise<Team> {
  await requestValidator(requestSchema.id, { id });
  const team = await TeamRepository.findTeamById(id);
  return TeamSerializer.serialize(team);
}

export async function updateTeam(
  id: string,
  teamRequest: TeamRequest
): Promise<void> {
  await requestValidator(requestSchema.id, { id });
  await requestValidator(requestSchema.team, teamRequest);
  await TeamRepository.updateTeam(id, teamRequest);
}

export async function removeTeam(id: string): Promise<void> {
  await requestValidator(requestSchema.id, { id });
  await TeamRepository.removeTeam(id);
}
