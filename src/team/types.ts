import League from '../league/entity';
import Member from '../member/entity';
import { MemberRequest } from '../member/types';
import Team from './entity';

export interface LeagueTeamRequest {
  team: TeamRequest;
  member: MemberRequest;
}

export interface TeamRequest {
  name: string;
  imageUrl: string;
}

export interface CustomTeamRepository {
  createAndSave: (
    member: Member,
    league: League,
    team: LeagueTeamRequest
  ) => Promise<void>;
  findTeamById: (id: string) => Promise<Team>;
  findTeamByName: (name: string) => Promise<Team>;
  updateTeam: (id: string, team: TeamRequest) => Promise<void>;
  removeTeam: (id: string) => Promise<void>;
  addManager: (team: Team, manager: Member) => Promise<void>;
}
