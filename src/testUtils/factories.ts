import faker from 'faker';
import Member from '../member/entity';
import { MemberRequest } from '../member/types';
import Role from '../role/entity';
import { Designation } from '../role/enum';

export const memberFactory = (
  attribute?: Record<string, string>
): MemberRequest => {
  return {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    password: 'passw0rD',
    imageUrl: faker.image.imageUrl(),
    ...attribute
  };
};

export const createMember = (
  factory: MemberRequest = memberFactory()
): Member => {
  const mbr = new Member();

  for (const key in factory) {
    mbr[key] = factory[key];
  }

  return mbr;
};

const designations = {
  owner: Designation.OWNER,
  manager: Designation.MANAGER,
  moderator: Designation.MODERATOR
};

type DesignationArg = 'owner' | 'manager' | 'moderator';

export const createRole = (designation: DesignationArg = 'manager'): Role => {
  const role = new Role();
  role.designation = designations[designation];
  return role;
};

export const generateRoleList = (
  ...designationArgs: DesignationArg[]
): Role[] => {
  return Array.from(designationArgs, (designation: DesignationArg) =>
    createRole(designation)
  );
};

export const generateMemberList = (length: number = 5): Member[] => {
  return Array.from({ length }, () => createMember());
};
