import apiDebug from './debug';

describe('Testing Debug function', () => {
  test('should be defined as a function', () => {
    expect(apiDebug).toBeInstanceOf(Function);
  });

  test('should return name of debugging function', () => {
    expect(apiDebug('test').namespace).toBe('draft-board-api:test');
  });
});
