import debug from 'debug';

export default function apiDebug(scope: string): debug.Debugger {
  return debug(`draft-board-api:${scope}`);
}
