import jwt, { SignOptions } from 'jsonwebtoken';
import Member from '../member/entity';
import crypto from 'crypto';
import { JwtResponse } from '../member/types';

export const privateKey = crypto.randomBytes(48).toString('hex');
function signedJwtMemberToken(member: Member): string {
  const opts: SignOptions = { expiresIn: '1h' };
  return jwt.sign({ id: member.id }, privateKey, opts);
}

export default function jwtResponse(
  fantasyDraftBoardMember: Member
): JwtResponse {
  const token = signedJwtMemberToken(fantasyDraftBoardMember);
  const resBody = {
    access_token: token,
    token_type: 'Bearer',
    expires_in: 3600
  };
  return resBody;
}
