import { Logger as TypeORMLogger, QueryRunner } from 'typeorm';
import koaPinoLogger from 'koa-pino-logger';
import pino, { Logger, LoggerOptions, PrettyOptions } from 'pino';
import DatabaseError from '../errors/databaseError';

const pinoPrettyOpts: PrettyOptions = {
  colorize: true,
  errorProps: '|',
  levelFirst: true
};

// TODO: write production logs to file
const pinoOpts: LoggerOptions = {
  name: 'FDB'
};

if (process.env.NODE_ENV !== 'production') {
  pinoOpts.transport = {
    target: 'pino-pretty',
    options: pinoPrettyOpts
  };
}

const logger: Logger = pino(pinoOpts);

export class DatabaseLogger implements TypeORMLogger {
  logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner) {
    const requestUrl = `${queryRunner?.data?.request?.url ?? ''} `;
    const queryMessage = `${requestUrl}executing query: ${query}`.trim();
    const parametersMessage = `${requestUrl}providing parameters: ${parameters}`.trim();

    logger.info(queryMessage);
    logger.info(parametersMessage);
  }

  logQueryError(
    error: string | Error,
    query: string,
    parameters?: any[],
    queryRunner?: QueryRunner
  ) {
    const requestUrl: string = `(${queryRunner?.data?.request?.url}) ` ?? '';
    const parametersMessage = `${requestUrl}providing parameters: ${parameters}`;
    const errMessage = new DatabaseError(
      `${requestUrl}executed query: ${query}`,
      error
    );

    logger.info(parametersMessage);
    logger.error(errMessage);
  }

  logQuerySlow(_time: number, _query: string) {}
  logSchemaBuild(_message: string) {}
  logMigration(_message: string) {}

  log(_level: 'log' | 'info' | 'warn' | 'error') {}
}

export const koaLogger = koaPinoLogger(pinoOpts);

export default logger;
