import Joi, { ObjectSchema } from 'joi';

export const member: ObjectSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string()
    .required()
    .min(8)
    .alphanum()
    .pattern(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]/,
      'minimum of one uppercase letter, one lowercase letter and one number'
    ),
  imageUrl: Joi.string()
    .required()
    .uri({ scheme: [/https?/] })
});

export const id: ObjectSchema = Joi.object({
  id: Joi.string().required().length(12).alphanum()
});

export const league: ObjectSchema = Joi.object({
  name: Joi.string().required(),
  imageUrl: Joi.string()
    .required()
    .uri({ scheme: [/https?/] })
});

export const team: ObjectSchema = Joi.object({
  name: Joi.string().required(),
  imageUrl: Joi.string()
    .required()
    .uri({ scheme: [/https?/] })
});

export const leagueTeam: ObjectSchema = Joi.object({
  team: team,
  member: member
});
