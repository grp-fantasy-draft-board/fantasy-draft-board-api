import { ObjectSchema } from 'joi';
import { DefaultContext } from 'koa';

export default async function requestValidator(
  schema: ObjectSchema,
  data: DefaultContext
): Promise<void> {
  await schema.validateAsync(data, { abortEarly: false });
}
