// @ts-check
/** @type {import('@stryker-mutator/api/core').PartialStrykerOptions} */
module.exports = {
  checkers: ['typescript'],
  cleanTempDir: true,
  coverageAnalysis: 'perTest',
  mutate: [
    'src/**/*.ts',
    '!src/**/*.test.ts',
    '!src/testUtils/*.ts',
    '!src/db/*',
    '!src/configs/*'
  ],
  mutator: { plugins: ['typescript'] },
  packageManager: 'yarn',
  reporters: ['clear-text', 'progress', 'json', 'html'],
  testRunner: 'jest',
  tempDirName: 'stryker-tmp',
  tsconfigFile: 'tsconfig.stryker.json',
  jest: {
    projectType: 'custom',
    configFile: 'jest.config.js',
    config: {
      testPathIgnorePatterns: ['node_modules', 'dist']
    },
    enableFindRelatedTests: true
  },
  htmlReporter: {
    baseDir: 'mutation/html'
  },
  jsonReporter: {
    fileName: 'mutation/mutation.json'
  }
};
